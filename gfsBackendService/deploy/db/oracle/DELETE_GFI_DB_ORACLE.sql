﻿-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2019 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------


DROP TABLE TBL_STATION;
DROP SEQUENCE TBL_STATION_ID_SEQ;

DROP TABLE TBL_GFI_ADDRESS;
DROP SEQUENCE TBL_GFI_ADDRESS_ID_SEQ;

DROP TABLE HTBL_FAILINFO_STATION;
DROP SEQUENCE HTBL_FAILINFO_STATION_ID_SEQ;

DROP TABLE HTBL_FAILURE_INFORMATION;
DROP SEQUENCE HTBL_FAILUREINFORMATION_ID_SEQ;

DROP TABLE tbl_failinfo_remind_mail_sent;
DROP SEQUENCE tbl_failinfo_reminsent_id_seq;

DROP TABLE tbl_failinfo_pub_channel;
DROP SEQUENCE tbl_fi_pub_channel_seq;

DROP TABLE tbl_failinfo_station;
DROP SEQUENCE tbl_fi_station_id_seq;

DROP TABLE tbl_failinfo_distgroup;
DROP SEQUENCE TBL_FAILINFO_DISTGROUP_ID_SEQ;

DROP TABLE TBL_DISTRIBUTION_GROUP_MEMBER;
DROP SEQUENCE TBL_DISTR_GROUP_MEMB_ID_SEQ;

DROP TABLE TBL_DISTRIBUTION_GROUP;
DROP SEQUENCE TBL_DISTRIBUTION_GROUP_ID_SEQ;

DROP TABLE TBL_FAILURE_INFORMATION;
DROP SEQUENCE TBL_FAILURE_INFORMATION_ID_SEQ;

DROP TABLE REF_RADIUS;
DROP SEQUENCE REF_RADIUS_ID_SEQ;

DROP TABLE REF_EXPECTED_REASON;
DROP SEQUENCE REF_EXPECTED_REASON_ID_SEQ;

DROP TABLE REF_GFI_BRANCH;
DROP SEQUENCE REF_GFI_BRANCH_ID_SEQ;

DROP TABLE REF_FAILURE_CLASSIFICATION;
DROP SEQUENCE REF_FAILURE_CLASS_ID_SEQ;

DROP TABLE REF_STATUS;
DROP SEQUENCE REF_STATUS_ID_SEQ;

DROP TABLE GFI_VERSION;
