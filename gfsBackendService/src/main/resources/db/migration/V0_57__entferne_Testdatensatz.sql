﻿-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2019 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------
-- Entferne Bernd Britzel Test-Datensätze
DELETE FROM public.tbl_failure_information_distribution_group WHERE fk_tbl_failure_information = (select id from public.tbl_failure_information where uuid = '37aef635-d0d4-4c47-ac25-c0d16c29e35c');
DELETE FROM public.tbl_failure_information_publication_channel  WHERE fk_tbl_failure_information = (select id from public.tbl_failure_information where uuid = '37aef635-d0d4-4c47-ac25-c0d16c29e35c');
DELETE FROM public.tbl_failure_information_reminder_mail_sent  WHERE fk_tbl_failure_information = (select id from public.tbl_failure_information where uuid = '37aef635-d0d4-4c47-ac25-c0d16c29e35c');

DELETE FROM public.tbl_failure_information WHERE uuid = '37aef635-d0d4-4c47-ac25-c0d16c29e35c';
