﻿-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2019 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------
DO $$
    BEGIN
        BEGIN
            ALTER TABLE public.TBL_DISTRIBUTION_GROUP ADD COLUMN EMAIL_SUBJECT varchar(255);
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column EMAIL_SUBJECT already exists in TBL_DISTRIBUTION_GROUP.';
        END;
    END;
$$
-- ALTER TABLE public.TBL_DISTRIBUTION_GROUP ADD COLUMN IF NOT EXISTS EMAIL_SUBJECT varchar(255); -- only postgres version > 9.6 available