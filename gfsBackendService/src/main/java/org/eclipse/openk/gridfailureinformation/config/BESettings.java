package org.eclipse.openk.gridfailureinformation.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class BESettings {
    @Value("${gridFailureInformation.maxListSize}")
    private int maxListSize;
}
