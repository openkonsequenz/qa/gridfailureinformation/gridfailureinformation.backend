/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import lombok.Data;


@Data
@Entity
public class TblFailinfoStation {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "tbl_fi_station_id_seq")
    @SequenceGenerator(name = "tbl_fi_station_id_seq", sequenceName = "tbl_fi_station_id_seq", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn( name = "fk_tbl_failure_information")
    private TblFailureInformation failureInformation;


    /*@ManyToOne
    @JoinColumn( name = "station_station_id")
    private TblStation station;*/
    private String stationStationId;

}
