/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.constants.Constants;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.mapper.AddressMapper;
import org.eclipse.openk.gridfailureinformation.model.TblAddress;
import org.eclipse.openk.gridfailureinformation.repository.AddressRepository;
import org.eclipse.openk.gridfailureinformation.viewmodel.AddressDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.HousenumberUuidDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeSet;
import java.util.UUID;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

@Service
public class AddressService {
    private final AddressRepository addressRepository;
    private final AddressMapper addressMapper;

    public AddressService(AddressRepository addressRepository, AddressMapper addressMapper) {
        this.addressRepository = addressRepository;
        this.addressMapper = addressMapper;
    }

    private static HousenumberUuidDto toHousenumberUuid(TblAddress tblAddress) {
        HousenumberUuidDto housenumberUuidDto = new HousenumberUuidDto();
        housenumberUuidDto.setHousenumber(tblAddress.getHousenumber());
        housenumberUuidDto.setUuid(tblAddress.getUuid());
        return housenumberUuidDto;
    }

    public List<AddressDto> getAddresses(Optional<String> branchOpt) {
        List<TblAddress> addressList;
        String branch = branchOpt.orElseGet(String::new);

        addressList = switch (branch) {
            case Constants.S -> addressRepository.findByPowerConnection(true);
            case Constants.G -> addressRepository.findByGasConnection(true);
            case Constants.W -> addressRepository.findByWaterConnection(true);
            case Constants.F -> addressRepository.findByDistrictheatingConnection(true);
            case Constants.TK -> addressRepository.findByTelecommConnection(true);
            default -> addressRepository.findAll();
        };

        return addressList.stream()
                .map(addressMapper::toAddressDto)
                .toList();
    }

    public AddressDto getAdressByUuid(UUID uuid) {
        TblAddress tblAddress = addressRepository.findByUuid(uuid)
                .orElseThrow(NotFoundException::new);
        return addressMapper.toAddressDto(tblAddress);
    }

    public List<String> getPostcodes(Optional<String> branchOpt) {
        List<String> postcodeList;
        String branch = branchOpt.orElseGet(String::new);

        postcodeList = switch (branch) {
            case Constants.S -> addressRepository.findAllPostcodesForPower();
            case Constants.G -> addressRepository.findAllPostcodesForGas();
            case Constants.W -> addressRepository.findAllPostcodesForWater();
            case Constants.F -> addressRepository.findAllPostcodesForDistrictheating();
            case Constants.TK -> addressRepository.findAllPostcodesForTelecomm();
            default -> addressRepository.findAllPostcodes();
        };

        return postcodeList;
    }


    public List<String> getCommunities(Optional<String> branchOpt) {
        List<String> communityList;
        String branch = branchOpt.orElseGet(String::new);

        communityList = switch (branch) {
            case Constants.S -> addressRepository.findAllCommunitysForPower();
            case Constants.G -> addressRepository.findAllCommunitysForGas();
            case Constants.W -> addressRepository.findAllCommunitysForWater();
            case Constants.F -> addressRepository.findAllCommunitysForDistrictheating();
            case Constants.TK -> addressRepository.findAllCommunitysForTelecomm();
            default -> addressRepository.findAllCommunitys();
        };

        return communityList.stream().filter(Objects::nonNull).filter(c -> !c.isEmpty()).collect(toCollection(ArrayList::new));
    }


    public List<String> getDistricts(String community, Optional<String> branchOpt) {
        List<String> districtList;
        String branch = branchOpt.orElseGet(String::new);

        districtList = switch (branch) {
            case Constants.S -> addressRepository.findDistrictsByCommunityForPowerConnections(community);
            case Constants.G -> addressRepository.findDistrictsByCommunityForGasConnections(community);
            case Constants.W -> addressRepository.findDistrictsByCommunityForWaterConnections(community);
            case Constants.F -> addressRepository.findDistrictsByCommunityForDistrictheatingConnections(community);
            case Constants.TK -> addressRepository.findDistrictsByCommunityForTelecommConnections(community);
            default -> addressRepository.findDistrictsByCommunity(community);
        };

        return districtList;
    }

    public List<String> getCommunities(String postcode, Optional<String> branchOpt) {
        List<String> communityList;
        String branch = branchOpt.orElseGet(String::new);

        communityList = switch (branch) {
            case Constants.S -> addressRepository.findCommunitiesByPostcodeForPowerConnections(postcode);
            case Constants.G -> addressRepository.findCommunitiesByPostcodeForGasConnections(postcode);
            case Constants.W -> addressRepository.findCommunitiesByPostcodeForWaterConnections(postcode);
            case Constants.F -> addressRepository.findCommunitiesByPostcodeForDistrictheatingConnections(postcode);
            case Constants.TK -> addressRepository.findCommunitiesByPostcodeForTelecommConnections(postcode);
            default -> addressRepository.findCommunitiesByPostcode(postcode);
        };

        return communityList;
    }


    public List<String> getPostcodes(String community, String district, Optional<String> branchOpt) {
        List<String> postcodeList;
        String branch = branchOpt.orElseGet(String::new);

        postcodeList = switch (branch) {
            case Constants.S -> addressRepository.findPostcodesByCommunityAndDistrictForPower(community, district);
            case Constants.G -> addressRepository.findPostcodesByCommunityAndDistrictForGas(community, district);
            case Constants.W -> addressRepository.findPostcodesByCommunityAndDistrictForWater(community, district);
            case Constants.F ->
                    addressRepository.findPostcodesByCommunityAndDistrictForDistrictheating(community, district);
            case Constants.TK -> addressRepository.findPostcodesByCommunityAndDistrictForTelecomm(community, district);
            default -> addressRepository.findPostcodesByCommunityAndDistrict(community, district);
        };
        return postcodeList;
    }


    public List<String> getDistricts(String postcode, String community, Optional<String> branchOpt) {
        List<String> districtList;
        String branch = branchOpt.orElseGet(String::new);

        districtList = switch (branch) {
            case Constants.S -> addressRepository.findDistrictsByPostcodeAndCommunityForPower(postcode, community);
            case Constants.G -> addressRepository.findDistrictsByPostcodeAndCommunityForGas(postcode, community);
            case Constants.W -> addressRepository.findDistrictsByPostcodeAndCommunityForWater(postcode, community);
            case Constants.F ->
                    addressRepository.findDistrictsByPostcodeAndCommunityForDistrictheating(postcode, community);
            case Constants.TK -> addressRepository.findDistrictsByPostcodeAndCommunityForTelecomm(postcode, community);
            default -> addressRepository.findDistrictsByPostcodeAndCommunity(postcode, community);
        };
        return districtList;
    }

    public List<String> getStreets(String postcode, String community, Optional<String> district, Optional<String> branchOpt) {
        List<String> streetList;
        String branch = branchOpt.orElseGet(String::new);

        if (district.isEmpty()) {
            streetList = switch (branch) {
                case Constants.S -> addressRepository.findStreetsByPostcodeAndCommunityForPower(postcode, community);
                case Constants.G -> addressRepository.findStreetsByPostcodeAndCommunityForGas(postcode, community);
                case Constants.W -> addressRepository.findStreetsByPostcodeAndCommunityForWater(postcode, community);
                case Constants.F ->
                        addressRepository.findStreetsByPostcodeAndCommunityForDistrictheating(postcode, community);
                case Constants.TK ->
                        addressRepository.findStreetsByPostcodeAndCommunityForTelecomm(postcode, community);
                default -> addressRepository.findStreetsByPostcodeAndCommunity(postcode, community);
            };
        } else {
            streetList = switch (branch) {
                case Constants.S ->
                        addressRepository.findStreetsByPostcodeAndCommunityAndDistrictForPower(postcode, community, district.get());
                case Constants.G ->
                        addressRepository.findStreetsByPostcodeAndCommunityAndDistrictForGas(postcode, community, district.get());
                case Constants.W ->
                        addressRepository.findStreetsByPostcodeAndCommunityAndDistrictForWater(postcode, community, district.get());
                case "F" ->
                        addressRepository.findStreetsByPostcodeAndCommunityAndDistrictForDistrictHeating(postcode, community, district.get());
                case "TK" ->
                        addressRepository.findStreetsByPostcodeAndCommunityAndDistrictForTelecomm(postcode, community, district.get());
                default ->
                        addressRepository.findStreetsByPostcodeAndCommunityAndDistrict(postcode, community, district.get());
            };
        }
        return streetList;
    }

    public List<HousenumberUuidDto> getHousenumbers(String postcode, String community, String street, Optional<String> branchOpt) {
        List<TblAddress> addressListList;
        String branch = branchOpt.orElseGet(String::new);

        addressListList = switch (branch) {
            case Constants.S ->
                    addressRepository.findByPostcodeAndCommunityAndStreetForPower(postcode, community, street);
            case Constants.G ->
                    addressRepository.findByPostcodeAndCommunityAndStreetForGas(postcode, community, street);
            case Constants.W ->
                    addressRepository.findByPostcodeAndCommunityAndStreetForWater(postcode, community, street);
            case Constants.F ->
                    addressRepository.findByPostcodeAndCommunityAndStreetForDistrictheating(postcode, community, street);
            case Constants.TK ->
                    addressRepository.findByPostcodeAndCommunityAndStreetForTelecomm(postcode, community, street);
            default -> addressRepository.findByPostcodeAndCommunityAndStreet(postcode, community, street);
        };
        return addressListList
                .stream()
                .map(AddressService::toHousenumberUuid)
                .collect(collectingAndThen(toCollection(() -> new TreeSet<>(Comparator.comparing(HousenumberUuidDto::getHousenumber))),
                        ArrayList::new));
    }
}
