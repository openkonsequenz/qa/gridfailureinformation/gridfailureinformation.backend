/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */

package org.eclipse.openk.gridfailureinformation.repository;

import org.eclipse.openk.gridfailureinformation.model.TblImportData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ImportDataRepository extends JpaRepository<TblImportData, Long > {

    List<TblImportData> findAll();

    Optional<TblImportData> findByUuid(UUID uuid);

    Optional<TblImportData> findByMetaId(String metaid);

    @Query("select count(*) from TblImportData")
    Long countImportData();
}
