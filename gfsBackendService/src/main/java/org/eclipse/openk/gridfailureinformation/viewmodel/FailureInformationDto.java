/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.viewmodel;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Data
public class FailureInformationDto implements Serializable {
    @JsonProperty("id")
    private UUID uuid;
    private String title;
    private String description;
    private Long versionNumber;
    private String responsibility;
    private String voltageLevel;
    private String pressureLevel;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date failureBegin;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date failureEndPlanned;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date failureEndResupplied;

    private String internalRemark;
    private String postcode;
    private String city;
    private String district;
    private String street;
    private String housenumber;
    private String stationId;
    private String stationDescription;
    private String stationCoords;
    private String freetextPostcode;
    private String freetextCity;
    private String freetextDistrict;
    private BigDecimal longitude;
    private BigDecimal latitude;
    private String objectReferenceExternalSystem;
    private String publicationStatus;
    private String publicationFreetext;
    private Boolean condensed;
    private Long condensedCount;
    private String faultLocationArea;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date createDate;
    private String createUser;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date modDate;
    private String modUser;

    private UUID failureClassificationId;
    private String failureClassification;

    private boolean isPlanned;

    private UUID failureTypeId;
    private String failureType;

    private UUID statusInternId;
    private String statusIntern;

    private String statusExtern;

    private UUID branchId;
    private String branch;
    private String branchDescription;
    private String branchColorCode;

    private UUID radiusId;
    private Long radius;

    private UUID expectedReasonId;
    private String expectedReasonText;

    private UUID failureInformationCondensedId;

    private List<StationPolygonDto> addressPolygonPoints;
    private List<UUID> stationIds;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof FailureInformationDto)) return false;

        FailureInformationDto that = (FailureInformationDto) o;

        return Objects.equals(isPlanned, that.isPlanned) && Objects.equals(uuid, that.uuid) && Objects.equals(title, that.title) && Objects.equals(description, that.description) && Objects.equals(versionNumber, that.versionNumber) && Objects.equals(responsibility, that.responsibility) && Objects.equals(voltageLevel, that.voltageLevel) && Objects.equals(pressureLevel, that.pressureLevel) && Objects.equals(failureBegin, that.failureBegin) && Objects.equals(failureEndPlanned, that.failureEndPlanned) && Objects.equals(failureEndResupplied, that.failureEndResupplied) && Objects.equals(internalRemark, that.internalRemark) && Objects.equals(postcode, that.postcode) && Objects.equals(city, that.city) && Objects.equals(district, that.district) && Objects.equals(street, that.street) && Objects.equals(housenumber, that.housenumber) && Objects.equals(stationId, that.stationId) && Objects.equals(stationDescription, that.stationDescription) && Objects.equals(stationCoords, that.stationCoords) && Objects.equals(freetextPostcode, that.freetextPostcode) && Objects.equals(freetextCity, that.freetextCity) && Objects.equals(freetextDistrict, that.freetextDistrict) && Objects.equals(longitude, that.longitude) && Objects.equals(latitude, that.latitude) && Objects.equals(objectReferenceExternalSystem, that.objectReferenceExternalSystem) && Objects.equals(publicationStatus, that.publicationStatus) && Objects.equals(publicationFreetext, that.publicationFreetext) && Objects.equals(condensed, that.condensed) && Objects.equals(condensedCount, that.condensedCount) && Objects.equals(faultLocationArea, that.faultLocationArea) && Objects.equals(createUser, that.createUser) && Objects.equals(modUser, that.modUser) && Objects.equals(failureClassificationId, that.failureClassificationId) && Objects.equals(failureClassification, that.failureClassification) && Objects.equals(failureTypeId, that.failureTypeId) && Objects.equals(failureType, that.failureType) && Objects.equals(statusInternId, that.statusInternId) && Objects.equals(statusIntern, that.statusIntern) && Objects.equals(statusExtern, that.statusExtern) && Objects.equals(branchId, that.branchId) && Objects.equals(branch, that.branch) && Objects.equals(branchDescription, that.branchDescription) && Objects.equals(radiusId, that.radiusId) && Objects.equals(radius, that.radius) && Objects.equals(expectedReasonId, that.expectedReasonId) && Objects.equals(expectedReasonText, that.expectedReasonText) && Objects.equals(failureInformationCondensedId, that.failureInformationCondensedId) && Objects.equals(addressPolygonPoints, that.addressPolygonPoints) && Objects.equals(stationIds, that.stationIds);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(uuid) ^ Objects.hashCode(title) ^ Objects.hashCode(description)^ Objects.hashCode(versionNumber)^ Objects.hashCode(responsibility)^ Objects.hashCode(voltageLevel)^ Objects.hashCode(pressureLevel)^ Objects.hashCode(failureBegin)^ Objects.hashCode(failureEndPlanned)^ Objects.hashCode(failureEndResupplied)^ Objects.hashCode(internalRemark)^ Objects.hashCode(postcode)^ Objects.hashCode(city)^ Objects.hashCode(district)^ Objects.hashCode(street)^ Objects.hashCode(housenumber)^ Objects.hashCode(stationId)^ Objects.hashCode(stationDescription)^ Objects.hashCode(stationCoords)^ Objects.hashCode(freetextPostcode)^ Objects.hashCode(freetextCity)^ Objects.hashCode(freetextDistrict)^ Objects.hashCode(longitude)^ Objects.hashCode(latitude)^ Objects.hashCode(objectReferenceExternalSystem)^ Objects.hashCode(publicationStatus)^ Objects.hashCode(publicationFreetext)^ Objects.hashCode(condensed)^ Objects.hashCode(condensedCount)^ Objects.hashCode(faultLocationArea)^ Objects.hashCode(createUser)^ Objects.hashCode(modUser)^ Objects.hashCode(failureClassificationId)^ Objects.hashCode(failureClassification)^ Objects.hashCode(isPlanned)^ Objects.hashCode(failureTypeId)^ Objects.hashCode(failureType)^ Objects.hashCode(statusInternId)^ Objects.hashCode(statusIntern)^ Objects.hashCode(statusExtern)^ Objects.hashCode(branchId)^ Objects.hashCode(branch)^ Objects.hashCode(branchDescription)^ Objects.hashCode(radiusId)^ Objects.hashCode(radius)^ Objects.hashCode(expectedReasonId)^ Objects.hashCode(expectedReasonText)^ Objects.hashCode(failureInformationCondensedId)^ Objects.hashCode(addressPolygonPoints) ^ Objects.hashCode(stationIds);
    }
}
