/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.viewmodel.FEInitialContentDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FESettingsDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class SettingsServiceTest {
    @Autowired
    private SettingsService settingsService;

    @Test
    public void shouldGetFESettings() {
        FESettingsDto feSettingsDto = settingsService.getFESettings();

        assertNotNull(feSettingsDto.getDetailMapInitialZoom());
        assertNotNull(feSettingsDto.getOverviewMapInitialZoom());
        assertNotNull(feSettingsDto.getExportChannels());
        assertNotNull(feSettingsDto.getOverviewMapInitialLatitude());
        assertNotNull(feSettingsDto.getOverviewMapInitialLongitude());
        assertNotNull(feSettingsDto.getDataExternInitialVisibility());
    }

    @Test
    public void shouldGetInitialEmailSubjectAndContent() {
        FEInitialContentDto feSettingsDto = settingsService.getInitialEmailSubjectAndContent();

        assertNotNull(feSettingsDto.getEmailContentCompleteInit());
        assertNotNull(feSettingsDto.getEmailSubjectCompleteInit());
        assertNotNull(feSettingsDto.getEmailContentPublishInit());
        assertNotNull(feSettingsDto.getEmailSubjectPublishInit());
        assertNotNull(feSettingsDto.getEmailSubjectUpdateInit());
        assertNotNull(feSettingsDto.getEmailContentUpdateInit());
    }
}
