/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.base;

import org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.UserInteractionTask;

public class UserInteractionTaskImpl extends UserInteractionTask<TestProcessSubject> {
    public boolean leaveStepCalled = false;
    public boolean enterStepCalled = false;
    public boolean stayInTask = false;

    public UserInteractionTaskImpl(String description ) {
        super(description);
    }

    @Override
    protected void onLeaveStep(TestProcessSubject model) throws ProcessException {
        this.leaveStepCalled = true;
    }

    @Override
    protected boolean isStayInThisTask(TestProcessSubject model){ // NOSONAR Parameter needed, if overridden
        return stayInTask;
    }

    @Override
    protected void onEnterStep(TestProcessSubject model) throws ProcessException {
        // implement empty
    }

    @Override
    public void enterStep(ProcessSubject model ) throws ProcessException {
        super.enterStep(model);
        this.enterStepCalled = true;
    }

}
