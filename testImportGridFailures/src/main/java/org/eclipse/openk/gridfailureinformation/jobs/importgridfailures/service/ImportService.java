/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.dtos.ForeignFailureMessageDto;
import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.exceptions.InternalServerErrorException;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.messaging.MessageChannel;

@Service
@Log4j2
@EnableConfigurationProperties
public class ImportService {
    private final MessageChannel failureImportChannel;

    private final ObjectMapper objectMapper;

    public ImportService(MessageChannel failureImportChannel, ObjectMapper objectMapper) {
        this.failureImportChannel = failureImportChannel;
        this.objectMapper = objectMapper;
    }

    public void pushForeignFailure(ForeignFailureMessageDto foreignFailureMessageDto) {
        try {
                failureImportChannel.send(
                        MessageBuilder.withPayload(
                                objectMapper.writeValueAsString(foreignFailureMessageDto.getPayload()))
                        .setHeader("metaId", foreignFailureMessageDto.getMetaId())
                        .setHeader("description", foreignFailureMessageDto.getDescription())
                        .setHeader("source", foreignFailureMessageDto.getSource())
                        .build());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new InternalServerErrorException("could.not.push.message");
        }
    }
}
