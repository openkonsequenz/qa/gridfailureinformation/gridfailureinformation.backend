/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.importadresses.service;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;
import org.eclipse.openk.gridfailureinformation.importadresses.util.UtmConverter;
import org.eclipse.openk.gridfailureinformation.importadresses.viewmodel.AddressDto;
import org.eclipse.openk.gridfailureinformation.importadresses.viewmodel.StationDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.xml.ws.http.HTTPException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.List;

@Service
@Log4j2
@Configuration
public class AddressImportService {
    @Value("${adressimport.file.addresses}")
    public String fileAddresses;
    @Value("${adressimport.file.power-connections}")
    public String filePowerConnections;
    @Value("${adressimport.file.water-connections}")
    public String fileWaterConnections;
    @Value("${adressimport.file.gas-connections}")
    public String fileGasConnections;
    @Value("${adressimport.file.districtheating-connections}")
    public String fileDistrictheatingConnections;
    @Value("${adressimport.file.telecommunication-connections}")
    public String fileTelecommunicationConnections;
    @Value("${adressimport.file.power-stations}")
    public String filePowerStations;

    public static final String DURATION = "Duration: ";

    private final AddressService addressService;

    private final StationService stationService;

    private final UtmConverter converter;

    protected interface LineConsumer {
        void accept( String[] line, AddressDto targetDto);
    }

    private final Object lock = new Object();
    private Boolean importInWork = Boolean.FALSE;

    public AddressImportService(AddressService addressService, StationService stationService, UtmConverter converter) {
        this.addressService = addressService;
        this.stationService = stationService;
        this.converter = converter;
    }

    @Async
    public void importAddresses(boolean cleanUp) {
        // Don't import twice
        synchronized (lock) {
            if (importInWork.equals(Boolean.TRUE)) {
                throw new HTTPException(HttpStatus.PROCESSING.value());
            }
            else {
                importInWork = Boolean.TRUE;
            }
        }

        if (cleanUp) {
            Instant cleanUpStart = Instant.now();
            log.info("CleanUp START");

            addressService.deleteAllAddresses();
            stationService.deleteAllStations();

            log.info("CleanUp ENDE");
            Instant cleanUpEnd = Instant.now();
            log.info("CleanUp Duration: " + Duration.between(cleanUpStart, cleanUpEnd));
        }

        Instant importStart = Instant.now();
        log.info("Import START");

        log.info("- Import Addresses START");
        importAddressFile(cleanUp);
        log.info("- Import Addresses END");

        log.info("- Import Power Connections START");
        importPowerConnectionsAddressFile();
        log.info("- Import Power Connections END");

        log.info("- Import Water Connections START");
        importWaterConnectionsAddressFile();
        log.info("- Import Water Connections END");

        log.info("- Import Gas Connections START");
        importGasConnectionsAddressFile();
        log.info("- Import Gas Connections END");

        log.info("- Import Districtheating Connections START");
        importDistrictheatingConnectionsAddressFile();
        log.info("- Import Districtheating Connections END");

        log.info("- Import Telecommunication Connections START");
        importTelecommunicationConnectionsAddressFile();
        log.info("- Import Telecommunication Connections END");

        log.info("- Import PowerStations START");
        importPowerStationFile(cleanUp);
        log.info("- Import PowerStations END");

        log.info("Import END");
        Instant importEnd = Instant.now();
        log.info("Total Import Duration: " + Duration.between(importStart, importEnd));

        synchronized (lock) {
            importInWork = Boolean.FALSE;
        }
    }


    public void importAddressFile(boolean isTableClean) {
        Instant start = Instant.now();
        File csvFile = new File(fileAddresses);

        CSVReader reader;
        try {
            final CSVParser parser = new CSVParserBuilder()
                                        .withSeparator(';')
                                        .withIgnoreQuotations(true)
                                        .build();
            if (!csvFile.exists()) {
                throw new IOException(String.format("CSV file with path %s not found", csvFile.getAbsolutePath()));
            }
            reader = new CSVReaderBuilder(new FileReader(csvFile))
                    .withSkipLines(1)
                    .withCSVParser(parser)
                    .build();
            String[]line;
            while ((line = reader.readNext()) != null) {
                storeAddress(isTableClean, line);
            }
        } catch (IOException | CsvValidationException | NumberFormatException e) {
            log.error(e);
        }
        Instant end = Instant.now();
        log.info(DURATION + Duration.between(start, end));
    }

    private void storeAddress(boolean isTableClean, String[] line) {
        AddressDto addressDto = addressFromLine(line);

        List<BigDecimal> decimals = converter.convertUTMToDeg(addressDto.getSdox1().doubleValue(), addressDto.getSdoy1().doubleValue());
        addressDto.setLongitude(decimals.get(0));
        addressDto.setLatitude(decimals.get(1));

        if (isTableClean) {
            addressService.insertAddress(addressDto);
        } else {
            addressService.updateOrInsertAddressByG3efid(addressDto);
        }
    }


    public void importPowerConnectionsAddressFile() {
        Instant start = Instant.now();
        String csvFile = filePowerConnections;

        parseFile(csvFile, (line, addressDto) -> {
            addressDto.setPowerConnection(true);
            addressDto.setStationId(line[8]);
        });

        Instant end = Instant.now();
        log.info(DURATION + Duration.between(start, end));
    }

    private void parseFile(String csvFile, LineConsumer action) {
        CSVReader reader = null;
        try {final CSVParser parser = new CSVParserBuilder()
                .withSeparator(';')
                .withIgnoreQuotations(true)
                .build();
            reader = new CSVReaderBuilder(new FileReader(csvFile))
                    .withSkipLines(1)
                    .withCSVParser(parser)
                    .build();
            String[]line;
            while ((line = reader.readNext()) != null) {
                AddressDto addressDto = addressFromLine(line);
                action.accept(line, addressDto);

                List<BigDecimal> decimals = converter.convertUTMToDeg( addressDto.getSdox1().doubleValue(), addressDto.getSdoy1().doubleValue());
                addressDto.setLongitude(decimals.get(0));
                addressDto.setLatitude(decimals.get(1));

                addressService.updateOrInsertAddressByG3efid(addressDto);
            }
        } catch (IOException | CsvValidationException | NumberFormatException e) {
            log.error(e);
        }
    }

    private AddressDto addressFromLine(String[] line) {
        AddressDto addressDto = new AddressDto();
        // !!! Parsing BigDecimal  anpassen, locale???
        addressDto.setSdox1(new BigDecimal(line[0].replace(",", ".")));
        addressDto.setSdoy1(new BigDecimal(line[1].replace(",", ".")));
        addressDto.setG3efid(Long.parseLong(line[2]));
        addressDto.setPostcode(line[3]);
        addressDto.setCommunity(line[4]);
        addressDto.setDistrict(line[5]);
        addressDto.setStreet(line[6]);
        addressDto.setHousenumber(line[7]);
        return addressDto;
    }


    public void importWaterConnectionsAddressFile() {
        Instant start = Instant.now();
        String csvFile = fileWaterConnections;

        parseFile(csvFile, (line, addressDto) -> {
            addressDto.setWaterConnection(true);
            addressDto.setWaterGroup(line[8]);
        });
        Instant end = Instant.now();
        log.info(DURATION + Duration.between(start, end));
    }

    public void importGasConnectionsAddressFile() {
        Instant start = Instant.now();
        String csvFile = fileGasConnections;

        parseFile(csvFile, (line, addressDto) -> {
            addressDto.setGasConnection(true);
            addressDto.setGasGroup(line[8]);
        });

        Instant end = Instant.now();
        log.info(DURATION + Duration.between(start, end));
    }

    public void importDistrictheatingConnectionsAddressFile() {
        Instant start = Instant.now();
        String csvFile = fileDistrictheatingConnections;

        parseFile(csvFile, (line, addressDto) -> addressDto.setDistrictheatingConnection(true));

        Instant end = Instant.now();
        log.info(DURATION + Duration.between(start, end));
    }

    public void importTelecommunicationConnectionsAddressFile() {
        Instant start = Instant.now();
        String csvFile = fileTelecommunicationConnections;

        parseFile(csvFile, (line, addressDto) -> addressDto.setTelecommConnection(true));

        Instant end = Instant.now();
        log.info(DURATION + Duration.between(start, end));
    }

    public void importPowerStationFile(boolean isTableClean) {
        Instant start = Instant.now();
        String csvFile = filePowerStations;

        CSVReader reader = null;
        try {final CSVParser parser = new CSVParserBuilder()
                .withSeparator(';')
                .withIgnoreQuotations(true)
                .build();
            reader = new CSVReaderBuilder(new FileReader(csvFile))
                    .withSkipLines(1)
                    .withCSVParser(parser)
                    .build();
            String[]line;
            while ((line = reader.readNext()) != null) {
                StationDto stationDto = new StationDto();
                // !!! Parsing BigDecimal  anpassen, locale
                stationDto.setSdox1(new BigDecimal(line[0].replace(",", ".")));
                stationDto.setSdoy1(new BigDecimal(line[1].replace(",", ".")));
                stationDto.setG3efid(Long.parseLong(line[2]));
                stationDto.setStationId(line[3]);
                stationDto.setStationName(line[4]);

                List<BigDecimal> decimals = converter.convertUTMToDeg( stationDto.getSdox1().doubleValue(), stationDto.getSdoy1().doubleValue());
                stationDto.setLongitude(decimals.get(0));
                stationDto.setLatitude(decimals.get(1));

                if (isTableClean) {
                    stationService.insertStation(stationDto);
                } else {
                    stationService.updateOrInsertStationByG3efid(stationDto);
                }

            }
        } catch (IOException | CsvValidationException | NumberFormatException e) {
            log.error(e);
        }
        Instant end = Instant.now();
        log.info(DURATION + Duration.between(start, end));
    }

}

