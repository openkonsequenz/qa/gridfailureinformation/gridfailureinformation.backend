/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.samointerface.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.samointerface.constants.Constants;
import org.eclipse.openk.gridfailureinformation.samointerface.dtos.ForeignFailureDataDto;
import org.eclipse.openk.gridfailureinformation.samointerface.dtos.ForeignFailureMessageDto;
import org.eclipse.openk.gridfailureinformation.samointerface.dtos.SAMOOutage;
import org.eclipse.openk.gridfailureinformation.samointerface.exceptions.InternalServerErrorException;
import org.eclipse.openk.gridfailureinformation.samointerface.mapper.SAMOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@EnableConfigurationProperties
public class ImportService {
    private final MessageChannel failureImportChannel;

    private final ObjectMapper objectMapper;

    private final SAMOMapper samoMapper;

    @Value("${gridFailureInformation.autopublish:false}")
    private boolean autopublish;
    @Value("${gridFailureInformation.onceOnlyImport:false}")
    private boolean onceOnlyImport;
    @Value("${gridFailureInformation.excludeEquals:true}")
    private boolean excludeEquals;
    @Value("${gridFailureInformation.excludeAlreadyEdited:true}")
    private boolean excludeAlreadyEdited;

    public ImportService(MessageChannel failureImportChannel, ObjectMapper objectMapper, SAMOMapper samoMapper) {
        this.failureImportChannel = failureImportChannel;
        this.objectMapper = objectMapper;
        this.samoMapper = samoMapper;
    }

    public void importSAMOOutage(String samoOutageJson) {
        try {
            SAMOOutage samoOutage = objectMapper.readValue(samoOutageJson, SAMOOutage.class);
            ForeignFailureMessageDto foreignFailureMessageDto = createForeignFailureMessageDto(samoOutage);
            log.info("ForeignFailureMessageDto created metaId: " + foreignFailureMessageDto.getMetaId());
            log.info("Succeeded");
        } catch (JsonProcessingException e) {
            log.error("Error parsing SAMOOutage", e);
        }

    }

    private ForeignFailureMessageDto createForeignFailureMessageDto(SAMOOutage samoOutage) {
        ForeignFailureMessageDto foreignFailureMessageDto = new ForeignFailureMessageDto();
        foreignFailureMessageDto.setMetaId(samoOutage.getId()); //fixme klärung
        foreignFailureMessageDto.setSource(Constants.SRC_SAMO);
        ForeignFailureDataDto foreignFailureDataDto = createForeignFailureData(samoOutage);
        foreignFailureMessageDto.setPayload(foreignFailureDataDto);
        return foreignFailureMessageDto;
    }

    private ForeignFailureDataDto createForeignFailureData(SAMOOutage samoOutage) {
        ForeignFailureDataDto foreignFailureDataDto = samoMapper.toForeignFailureDataDto(samoOutage);
        foreignFailureDataDto.setAutopublish(autopublish);
        foreignFailureDataDto.setOnceOnlyImport(onceOnlyImport);
        foreignFailureDataDto.setExcludeEquals(excludeEquals);
        foreignFailureDataDto.setExcludeAlreadyEdited(excludeAlreadyEdited);
        foreignFailureDataDto.setBranch(Constants.BRANCH_ELECTRICITY); //fixme klärung: logik einbauen
        foreignFailureDataDto.setPlanned(false); //fixme klärung
        return foreignFailureDataDto;
    }

    public void pushForeignFailure(ForeignFailureMessageDto foreignFailureMessageDto) {

        try {
            failureImportChannel.send(
                    MessageBuilder.withPayload(
                            objectMapper.writeValueAsString(foreignFailureMessageDto.getPayload()))
                            .setHeader("metaId", foreignFailureMessageDto.getMetaId())
                            .setHeader("description", foreignFailureMessageDto.getDescription())
                            .setHeader("source", foreignFailureMessageDto.getSource())
                            .build());
            log.info("Succesfully sent message from SAMO-Interface to main-app with metaId: " + foreignFailureMessageDto.getMetaId());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new InternalServerErrorException("could.not.push.message");
        }
    }
}
