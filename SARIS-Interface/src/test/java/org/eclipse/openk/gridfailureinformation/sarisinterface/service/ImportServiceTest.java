/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.sarisinterface.service;

import org.eclipse.openk.gridfailureinformation.sarisinterface.SarisInterfaceApplication;
import org.eclipse.openk.gridfailureinformation.sarisinterface.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.sarisinterface.constants.Constants;
import org.eclipse.openk.gridfailureinformation.sarisinterface.dtos.ForeignFailureMessageDto;
import org.eclipse.openk.gridfailureinformation.sarisinterface.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl.GetAktuelleGVUsInfoAllgemeinResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = SarisInterfaceApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class ImportServiceTest {

    @Autowired
    private ImportService importService;

    @MockBean
    SarisWebservice sarisWebservice;

    @Test
    public void shoulImportUserNotification() {

        ImportService importServiceSpy = spy(this.importService);

        GetAktuelleGVUsInfoAllgemeinResponse mockedSarisResponse = MockDataHelper.getMockedSarisResponse(
                "sarisRealMockResponse.xml");

        when(sarisWebservice.getAktuelleGVU(anyInt(), anyBoolean())).thenReturn(mockedSarisResponse);

        int mockedSarisResponseListSize = mockedSarisResponse.getGetAktuelleGVUsInfoAllgemeinResult().getViewGeplanteVU().size();

        importServiceSpy.importForeignFailures(Constants.SARIS_ELECTRICITY_BRANCH_ID, false, false);

        verify(importServiceSpy, times(mockedSarisResponseListSize)).pushForeignFailure(any(ForeignFailureMessageDto.class));
    }


}
