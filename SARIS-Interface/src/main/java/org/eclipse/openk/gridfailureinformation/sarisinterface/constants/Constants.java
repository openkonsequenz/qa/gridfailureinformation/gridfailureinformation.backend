/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.sarisinterface.constants;
public final class Constants {

    public static final String SRC_SARIS = "SARIS";

    public static final String BRANCH_WATER = "W";
    public static final String BRANCH_GAS = "G";
    public static final String BRANCH_ELECTRICITY = "S";
    public static final String BRANCH_OTHER = "OS";

    public static final int SARIS_GAS_BRANCH_ID = 2898;
    public static final int SARIS_ELECTRICITY_BRANCH_ID = 2899;
    public static final int SARIS_WATER_BRANCH_ID = 2900;



    public static final String VOLTAGE_LVL_LOW = "NS";

    public static final String PLANNED_OUTAGE = "planned";
    public static final String UNPLANNED_OUTAGE = "unplanned";

    private Constants() {
        // empty Constructor for the sake of SONAR
    }
}
